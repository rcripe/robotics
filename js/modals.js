$(document).ready(function() {
  // General modal functionality
  $('#openHcpModal').click(function(e) {
    e.preventDefault();
    $('.js-hcp-modal').fadeIn();
    $('.js-hcp-modal-overlay').addClass('in');
    $('.js-hcp-modal-overlay').css('display', 'block');
  });

  $('.js-close-hcp-modal').click(function() {
    $(".js-hcp-modal").fadeOut();
    $(".js-hcp-modal-overlay").css('display', 'none');
  });

  $('.js-open-video-overlay').on('click', function(e) {
    e.preventDefault();
    $('#videoOverlayContainer').fadeIn(200);
    $('.js-video-modal').fadeIn();
    $('.js-video-modal-overlay').addClass('in');
    $('.js-video-modal-overlay').css('display', 'block');
    $('body').addClass('modal-open');
  });

  $('.js-close-video-overlay').click(function() {
      $('#videoOverlayContainer').fadeOut(200);
      $('.js-video-modal').fadeOut();
      $('.js-video-modal-overlay').css('display', 'none');
      $('body').removeClass('modal-open');
  });

  // Gateway Page
  $('#openGatewayPageModal').click(function(e) {
    e.preventDefault();
    $('.js-gateway-page-modal').fadeIn();
    $('.js-gateway-page-overlay').addClass('in');
    $('.js-gateway-page-overlay').css('display', 'block');
  });

  $('.js-close-gateway-page-modal').click(function() {
      $(".js-gateway-page-modal").fadeOut();
      $(".js-gateway-page-overlay").css('display', 'none');
  });
});
