/* _____________________________________________

               FORM VALIDATION
______________________________________________ */
function validateContactForm() {
  return CONTACT.validateForm();
}

;CONTACT = {

  validateForm: function() {

    var isValid       = false;
    var allCheckboxes = [];
    var allFormFields = [];

    // find all input and select dropdown fields
    $(".js-validate-contact-form input, .js-validate-contact-form select").each(function() {
      var field = $(this);
      if (field.attr('aria-required') == 'true') {
        // push fields into arr
        allFormFields.push(field);
      }
    });

    var myCheckboxes = [];
    // find all checkboxes in form
    $(".js-validate-contact-form input[type='checkbox']").each(function() {
      var cbInput = $(this);
      // push checkboxes into arr
      myCheckboxes.push(cbInput);
    });

    var checkboxesChecked = [];
    $(".js-validate-contact-form input[type='checkbox']:checked").each(function() {
      if ( $(this).attr('aria-required') == 'true' ) {
        var cbInput = $(this);
        checkboxesChecked.push(cbInput);
      }
    });

    function isChecked(element, index, array) {
      return element.checked != false;
    }

    function hasAValue(element, index, array) {
      return element.attr('aria-required') != 'true' || element.attr('aria-required') == 'true' && element.val() != "" && element.val() != null;
    }

    if ( myCheckboxes.length > 0 && myCheckboxes.length == checkboxesChecked.length && myCheckboxes.every(hasAValue) && allFormFields.every(hasAValue)
      || myCheckboxes.length <= 0 && allFormFields.every(hasAValue) ) {

      // submit form
      isValid = true;
    } else {
      $(".js-form-field-error-message").css("display", "block");
      myCheckboxes.forEach(function(element) {
        if (element.attr('aria-required') == 'true') {
          if (element.is(':checked') != false) {
            element.next().css('color', '#121212');
          }
          else {
            element.next().css('color', '#ed002a');
          }
        }
      });

      // loop through to find empty elements and add border color
      allFormFields.forEach(function(element) {
        if (element.attr('aria-required') != 'true' || element.attr('aria-required') == 'true' && element.val() != "" && element.val() != null)  {
          element.css("border", "1px solid #b1b3b3");
        } else {
          element.css("border", "1px solid #ed002a");
        }
      });

      // do not submit form
      isValid = false;
    }

    if (isValid === true) {
      return true;
    } else {
      return false;
    }
  }
}
