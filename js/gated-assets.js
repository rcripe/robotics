/* ____________________________________________________

              GATED ASSETS FLYOUT
_____________________________________________________ */
var gatedAssetsFlyout = document.getElementById("gatedAssetsFlyout");
var gatedAssetsFlyout_overlay = document.getElementById("gatedAssetsOverlay");
var gatedAssetsFlyout_openBtn = $(".js-open-gated-assets-flyout");
var gatedAssetsFlyout_closeBtn = document.getElementsByClassName("js-close-gated-assets-flyout");
var gatedAssetsFlyout_footer = $(".gated-assets-form__footer");

// opens gated assets flyout
$(gatedAssetsFlyout_openBtn).on("click", function() {
  openGatedAssetsFlyout();
});

function openGatedAssetsFlyout()
{
  $(gatedAssetsFlyout).addClass("animate");
  $(gatedAssetsFlyout_overlay).fadeIn(300).removeClass("is-hidden");
}

// closes gated assets flyout
$(gatedAssetsFlyout_closeBtn).on("click", function() {
  closeGatedAssetsFlyout();
});

function closeGatedAssetsFlyout()
{
  // $(gatedAssetsFlyout_footer).css("position", "absolute");
  $(gatedAssetsFlyout).removeClass("animate");
  $(gatedAssetsFlyout_overlay).fadeOut(300).addClass("is-hidden");
}

// closes gated assets flyout if click is outside of main area
$(gatedAssetsFlyout_overlay).mousedown(function(e)
{
  var clicked = $(e.target); // element clicked
  if (clicked.is(gatedAssetsFlyout) || clicked.parents().is(gatedAssetsFlyout)) {
    //click happened within menu, do nothing
    return;
  } else {
    //click was outside menu, so close it
   closeGatedAssetsFlyout();
   }
});





/* ____________________________________________________

                 DATA LOOK-UP IN ELOQUA
_____________________________________________________ */

// ---------------------------------------- POPULATE FIELDS FROM ELOQUA RECORD FOUND USING EMAIL ADDRESS

/*
 General tracking scripts have to be placed on the page where you're
 intending to perform the data lookup.
 This is the beginning of general asynch tracking scripts.
*/

var _elqQ = _elqQ || [];
_elqQ.push(['elqSetSiteId', '298548211']);
_elqQ.push(['elqTrackPageView']);
(function () {
   function async_load() {
       var s = document.createElement('script');
       s.type = 'text/javascript';
       s.async = true;
       s.src = 'http://img.en25.com/i/elqCfg.min.js';
       var x = document.getElementsByTagName('script')[0];
       x.parentNode.insertBefore(s, x);
   }
   if (window.attachEvent) { window.attachEvent('onload', async_load); }
   else { window.addEventListener('DOMContentLoaded', async_load, false); }
})();


function lookUpData() {
  _elqQ.push(['elqDataLookup', escape('9c54b50a-5347-4357-ab5c-1d2555a919fb'),'<C_EmailAddress>'+$('#EmailAddress').val()+'</C_EmailAddress>']);
}


// Looks up data about existing contact using email address if it's prefilled from cookies

window.addEventListener('load', function () {
   if($('#EmailAddress').val()) {
      lookUpData();
   }
});


// Looks up data about existing contact as email address is typed

$('#EmailAddress').bind('keyup change', function() {
  lookUpData();
});


// All values come from Eloqua Data Lookup Script (Find all the field values in Eloqua under Web Data Lookup > EU Contact Data Lookup)

var prepops = [
{ id : "firstName", val : "C_FirstName" },
{ id : "lastName", val : "C_LastName" },
{ id : "country", val : "C_Country"},
{ id : "jobRole", val : "C_Job_Role1"}
];


// Populate fields with the data that was found for that record

function SetElqContent()
{
   if(this.GetElqContentPersonalizationValue) // If a record was found
   {
       prepops.forEach(function(prepop) {
           GetElqContentPersonalizationValue(prepop.val) && $('#' + prepop.id).val( GetElqContentPersonalizationValue(prepop.val));
       });
   }
}
