$(function() {
    var $window = $(window);
    var $bg_animations = $('.animated-circle');

    function animate_once() {
      var tolerancePixelTop = 400;
      var tolerancePixelBottom = 100;
      var scrollTop = $(window).scrollTop() + tolerancePixelTop;
      var scrollBottom = $(window).scrollTop() + $(window).height() - tolerancePixelBottom;
      var window_height = $window.height();
      var window_top_position = scrollTop;
      var window_bottom_position = scrollBottom;

      $.each($bg_animations, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
        // var myComponent = $element.closest('section');
        // $(".modular-copy").hide();

        //check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
          (element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
          // setTimeout(function() {
          //   $(myComponent).addClass("theme--black");
          //   $(".modular-copy").fadeIn(3000);
          // }, 2000);
        } else {
          // do nothing
        }
      });
    }

    $window.on('scroll resize', animate_once);
    $window.trigger('scroll');
});
