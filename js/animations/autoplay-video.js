$(function() {
    var $window = $(window);
    var loopingVideo = $('video.js-plays-in-view');

    function loopVideoWhileInView() {
      if ($(window).width() < 501) {
        tolerancePixel = 100;
      } else if ($(window).width() >= 501) {
        tolerancePixel = 300;
      }

      var scrollTop = $(window).scrollTop() + tolerancePixel;
      var scrollBottom = $(window).scrollTop() + $(window).height() - tolerancePixel;

      loopingVideo.each(function(index, el) {
        var yTopMedia = $(this).offset().top;
        var yBottomMedia = $(this).height() + yTopMedia;

        if(scrollTop < yBottomMedia && scrollBottom > yTopMedia){
            $(this).addClass('in-view');
            $(this).get(0).play();
        } else {
            $(this).get(0).pause();
            $(this).removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', loopVideoWhileInView);
    $window.trigger('scroll');


    var playOnceVideo = $("video.js-plays-once-in-view");
    var myPlayOnceVideo = document.querySelector("video.js-plays-once-in-view");

    console.log("play once video: ", playOnceVideo);
    console.log("my video: ", myPlayOnceVideo);

    myPlayOnceVideo.onended = (event) => {
      console.log('Video stopped either because 1) it was over, ' +
          'or 2) no further data is available.');
    };

    myPlayOnceVideo.loop = false;

    function playVideoOnceWhileInView() {

      var scrollTop = $(window).scrollTop();

      playOnceVideo.each(function(index, el) {
        var yTopMedia = $(this).offset().top;

        if (scrollTop > yTopMedia) {
          $(this).get(0).play();
        }
      });
    }
    
    $window.on('scroll', playVideoOnceWhileInView);
});
