/* ____________________________________________________

                    USER LOGIN
_____________________________________________________ */
var userLogin = document.getElementById("userLogin");
var activeUser = document.getElementById("activeUser");
var userLoggedIn = false;
// updates user login appearance
if (userLoggedIn) {
  activeUser.style.display = "flex";
  userLogin.style.display = "none";
} else {
  activeUser.style.display = "none";
  userLogin.style.display = "flex";
}



/* ____________________________________________________

              CONTACT & SUPPORT MENU
_____________________________________________________ */
var supportMenu = document.getElementById("supportMenu");
var openSupportMenuBtn = document.getElementById("openSupportMenu");
var closeSupportMenuBtn = document.getElementById("closeSupportMenu");

// opens support menu
openSupportMenuBtn.addEventListener("click", function() {
  openSupportMenu();
});

function openSupportMenu()
{
  hideMenu();
  $(".comp__medsurg-header").removeClass("is-positioned");
  $(".js-support-menu").addClass("animate");
  $(".js-overlay").fadeIn(300).removeClass("is-hidden");
  $("body").css("overflow-y", "hidden");
}

// closes support menu
closeSupportMenuBtn.addEventListener("click", function() {
  closeSupportMenu();
});

function closeSupportMenu()
{
  $(".js-support-menu").removeClass("animate");
  $(".js-overlay").fadeOut(300).addClass("is-hidden");
  $("body").css("overflow-y", "initial");
  $(".comp__medsurg-header").addClass("is-positioned");
}

// closes support menu if click is outside of support menu area
$(".js-overlay").mousedown(function(e)
{
  var clicked = $(e.target); // element clicked
  if (clicked.is(".js-support-menu") || clicked.parents().is(".js-support-menu")) {
    //click happened within menu, do nothing
    return;
  } else {
    //click was outside menu, so close it
    closeSupportMenu();
   }
});



/* ____________________________________________________

                      SEARCHBAR
_____________________________________________________ */
// expands searchbox
$(document).ready(function() {
  $("#searchbox").click(function() {
    expandSearchbox();
  });

  $(".js-expand-searchbox").on("click", function() {
    showMobileSearchbarContainer();
    expandSearchbox();
  });

  function expandSearchbox() {
    // show cancel search button
    $('#cancelSearch').removeClass("is-hidden");
    // expand searchbox
    $("#searchbox.is-collapsed").removeClass("is-collapsed");
    $("#searchbox").addClass("is-expanded");
  }

  function showMobileSearchbarContainer() {
    $(".searchbar").addClass("is-open");
  }
});

// collapses searchbox
$("#cancelSearch").click(function() {
// put searchbox back to original style
  $("#searchbox.is-expanded").removeClass("is-expanded");
  $("#searchbox").addClass("is-collapsed");
  // hides cancel button
  $("#cancelSearch").addClass("is-hidden");

  if ( $(".searchbar").hasClass("is-open") ) {
    $(".searchbar").removeClass("is-open");
  }
});









/* ____________________________________________________

              COUNTRY SELECTOR MENUS
_____________________________________________________ */
// country selector on desktop
var regionSelector = document.getElementById("regionSelector");
// shows menu on desktop
regionSelector.addEventListener("click", function() {
  showRegions();
});
// current country desktop
var currentCountry = document.getElementById("currentCountry");

// country selector on tablet/mobile
var triggerCountrySelect = document.getElementById("triggerCountrySelect");
// shows menu on tablet/mobile
triggerCountrySelect.addEventListener("click", function() {
  showRegions();
});

// currentCountry mobile
var mobileCurrentCountry = document.getElementById("mobileCurrentCountry");

// opens country selector
function showRegions()
{
  $(".comp__medsurg-header").removeClass("is-positioned");
  $(".js-regions").addClass("animate");
  $(".js-overlay").fadeIn(300).removeClass("is-hidden");
  $("body").css("overflow-y", "hidden");
}

// closes country selector
function hideRegions()
{
  $(".js-regions").removeClass("animate");
  $(".js-overlay").fadeOut(300).addClass("is-hidden");
  $("body").css("overflow-y", "initial");
  $(".comp__medsurg-header").addClass("is-positioned");
}

// closes menu
$(".js-hide-regions").on("click", function(e) {
  e.preventDefault();
  hideRegions();
});

$(".js-region").on("click", function() {
  // changes styles on current region
  $(this).addClass("is-active");
  $(this).siblings().removeClass("is-active");
  // updates menu items with current region
  var myCountry = $(this).children("a").attr("data-country");
  currentCountry.innerHTML = myCountry;
  mobileCurrentCountry.innerHTML = myCountry;
});

// closes menu if user clicks outside of menu
$(".js-overlay").mousedown(function(e)
{
  var clicked = $(e.target); // element clicked
  if (clicked.is(".js-regions") || clicked.parents().is(".js-regions")) {
    //click happened within regions, do nothing
    return;
  } else {
    //click was outside regions, so close it
    hideRegions();
   }
});



/* ____________________________________________________

                    MAIN MENU
_____________________________________________________ */
// onload
$(".mobile-menu-controller > .menu__icon").addClass("js-show-menu");
$(".mobile-menu-controller > .menu__icon > .mi").addClass("mi-btb-bars");

// opens hamburger menu on tablet/mobile
$(".js-show-menu").on("click", function(e) {
  e.preventDefault();
  showMenu();
});
// closes hamburger menu
$(".js-hide-menu").on("click", function(e) {
  console.log("js hide menu click");
  e.preventDefault();
  hideMenu();
});

$(".mobile-menu-controller").on("click", function() {
  console.log("mobile menu controller click");
  if ( $(".mobile-menu-controller").children(".mi").hasClass("mi-btb-bars") ) {
    // open menu
    showMenu();
  } else {
    // close menu
    hideMenu();
  }
});

// opens hamburger menu
function showMenu()
{
  console.log("open the menu");
  $(".mobile-menu-controller").children(".mi").removeClass("mi-btb-bars").addClass("mi-btb-times");
  $(".js-header-menu").addClass("animate");
  $(".comp__medsurg-header").addClass("mobile-style");
  $("body").css("overflow-y", "hidden");
}

// closes hamburger menu
function hideMenu()
{
  console.log("close the menu");
  $(".mobile-menu-controller").children(".mi").removeClass("mi-btb-times").addClass("mi-btb-bars");
  $(".js-header-menu").removeClass("animate");
  $(".comp__medsurg-header").css("z-index", "1");
  $(".comp__medsurg-header").removeClass("mobile-style");
  $("body").css("overflow-y", "initial");
}
