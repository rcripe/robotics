var fixedNavMenuToggle = document.getElementById("fixedNavMenuToggle");
var fixedNavMenu = document.getElementById("fixedNavMenu");
var fixedNavMenuItem = $(fixedNavMenu).children('li');
var fixedNavLinks = document.getElementById("fixedNavLinks");
var fixedNavWrapper = document.getElementById("fixedNavWrapper");
var fixedNavComp = $('.comp__fixed-navigation');
var modularFixedNav = $('.modular-fixed-nav');

if (fixedNavComp.length) {
  // shows menu
  fixedNavMenuToggle.addEventListener("click", function(e) {
    e.preventDefault();

    var myModularFixedNav = $(this).closest('.modular-fixed-nav');

    if ( $(fixedNavMenu).hasClass("is-open") ) {
      $(fixedNavLinks).removeClass("is-open");
      $(fixedNavMenu).removeClass("is-open");
      $(fixedNavWrapper).removeClass("is-open");
      $(this).html("arrow_drop_down");
    } else {
      $(fixedNavLinks).addClass("is-open");
      $(fixedNavMenu).addClass("is-open");
      $(fixedNavWrapper).addClass("is-open");
      $(this).html("arrow_drop_up");
    }
  });

  var stickyNav = function() {
    var fixedNavCompTop = $(modularFixedNav).offset().top;
    var scrollTop = $(window).scrollTop();

    if (scrollTop > fixedNavCompTop) {
      $(modularFixedNav).addClass('has-fixed-styles');
    } else {
      $(modularFixedNav).removeClass('has-fixed-styles');
    }
  };

  stickyNav();

  $(window).scroll(function() {
    stickyNav();
  });

  $(window).resize(function() {
    stickyNav();
  });
}
