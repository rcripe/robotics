var tab = (".js-tabbed-content-tabs > .js-tab");

$(tab).on("click", function(e) {
  e.preventDefault();

  // add active styles
  $(this).addClass("is-active");
  // remove active styles from siblings
  $(this).siblings().removeClass("is-active");
  // differentiate from any same components on the page
  var currentSet = $(this).parent().parent().parent();
  // get data-tab value for clicked item
  var currentTab = $(this).data("tab");
  // hide tabs that don't match data-tab value
  $(currentSet).find(".js-tabbed-content-container:not([data-tab='" + currentTab + "'])").hide();
  // show the matching tab
  $(currentSet).find(".js-tabbed-content-container[data-tab='" + currentTab + "']").fadeIn(750);
});
