/* Enable the Lightbox */
function enableLightbox(type, width, height, source, title) {
	var winWidth = parseInt($(window).width());
	var winHeight = parseInt($(window).height());
	var mediaContainerWidth = 0;
	var mediaContainerHeight = 0;
	
	/* build the generic lightbox for media to be displayed inside */
	$('body').append($('<div class="lightbox-wrapper">' +
			'<div class="lightbox-container">' +
				'<div class="lightbox-background" onclick="disableLightbox();">' +
				'</div>' +
				'<div class="close-button" onclick="disableLightbox();">' +
					'<i class="mi-btr-times"></i>' +
				'</div>' +
				'<div class="media-container">' +
					'<div class="lightbox-media">' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>')
    );
    
	if (width > winWidth) {
		mediaContainerWidth = winWidth;
	} else {
		mediaContainerWidth = width;
	}
	$('.media-container').css('width', mediaContainerWidth);
	
	/* if a title is passed, build the lightbox title */
	if (title != null && title != "") {
		buildLightboxTitle(title);
	}
	
	/* get lightbox type and build correct inner media */
	if (type == 'scene7') {	
		if (width > winWidth) {
			buildS7videoPlayer(winWidth, ((winWidth / width) * height), source);
		} else {
			buildS7videoPlayer(width, height, source);
		}
	}
		
	/* show the lightbox */
	$('.lightbox-wrapper').toggle('slide', {direction: 'up', duration:900, easing:'easeOutExpo'}, function(){
		/* set height based on height of media and title inside the lightbox */
		var lightboxMediaHeight = parseInt($('.lightbox-media').outerHeight());
		var lightboxTitleHeight = parseInt($('.lightbox-title').outerHeight());
		var totalHeight = lightboxMediaHeight + lightboxTitleHeight;
		if (totalHeight > winHeight) {
			var heightRatio = winHeight / totalHeight;
			$('.media-container').css('width', mediaContainerWidth * heightRatio);
			$('.media-container').css('height', winHeight);
		} else {
			$('.media-container').css('height', totalHeight);
		}
	});
}

/* Disable the Lightbox */
function disableLightbox() {
	/* if lightbox had a video in it, need to unload video due to Chrome bug */
	if ($('.lightbox-media .videoplayer')) {
		$('#videoplayervideo').get(0).pause();
		$('#videoplayervideo').prop('src', false);
		$('#videoplayervideo').get(0).load();
	}
	
	/* hide the lightbox and remove it */
	$('.lightbox-wrapper').toggle('slide', {direction: 'up', duration:900, easing:'easeOutExpo'}, function(){
		$('body').find('.lightbox-wrapper').first().remove();
	});
}

/* Build Lightbox Title */
function buildLightboxTitle(title) {
	$('.media-container').append($('<div class="lightbox-title">' +
		  '<h5>' + title + '</h5>' +
	  '</div>')
	);
}

/* Build Scene7 Videoplayer */
function buildS7videoPlayer(width, height, source) {
	$('.lightbox-media').append($('<div class="videoplayer">' +
		 '<video id="videoplayervideo" width="' + width + 'px" height="' + height + 'px" autoplay preload="none" style="width: 100%; height: 100%;">' +
			 '<source src="' + source + '" type="video/mp4"/>' +
		 '</video>' +
		 '<script type="text/javascript">$("#videoplayervideo").mediaelementplayer({defaultVideoWidth:' + width + ',defaultVideoHeight:' + height + ',startVolume:0.8,loop:false,enableAutosize:true,features:["playpause","progress","current","duration","tracks","volume","fullscreen"],alwaysShowControls:false,iPadUseNativeControls:true,iPhoneUseNativeControls:true,AndroidUseNativeControls:true,alwaysShowHours:false,showTimecodeFrameCount:false,framesPerSecond:24,enableKeyboard:false,pauseOtherPlayers:true});</script>' +
	 '</div>')
	);
}
