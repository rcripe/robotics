/*
	Attributed to: Simon Willison
	http://blog.simonwillison.net/search/addloadevent

	If window.onload has not already been assigned a function,
	the function passed to addLoadEvent is simply assigned to
    window.onload. If window.onload has already been set, a
    brand new function is created which first calls the
    original onload handler, then calls the new handler
    afterwards.

    addLoadEvent(nameOfSomeFunctionToRunOnPageLoad);

    addLoadEvent(function() {
		example code here
	});

	NOTE: you need to put the addLoadEvent function into a document.ready
    	for it to run, otherwise you need to implicitly call it.
	    	-Tyrell Baldwin

        $(document).ready(function() {
			addLoadEvent(function() {
				imageAspect();
			});
		});

*/

function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}

/*
	Number padder for adding 0's to numbers.
	Select your number, and then select a number of total digits.
		pad(1,3); becomes 001
		pad(32,5); becomes 00032.

	Author:		Tyrell Baldwin
*/
function pad(number, length) {
	var str = '' + number;
	while (str.length < length) {
		str = '0' + str;
	}
	return str;
}

